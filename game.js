const tower_1 = document.getElementById("tower_1");
const tower_2 = document.getElementById("tower_2");
const tower_3 = document.getElementById("tower_3");


let playerMode = "Pickup";
let diskHolder = null;

const diskPickedFromSourceTower = function (pickedUpDisk) {
    let numberOfDisk = pickedUpDisk.childElementCount;
    console.log(numberOfDisk);
    if (numberOfDisk === 0) {
        alert('You clicked empty tower!')
    } else {
        diskHolder = pickedUpDisk.lastElementChild;
        playerMode = "Release"
    }
    win();
}

const isPickupIsputToDestinationTower = function (event) {
    console.log(`player ${playerMode} disk`);
    if (playerMode === "Pickup") {
        diskPickedFromSourceTower(event.currentTarget);
    } else if (playerMode === "Release") {
        releaseToDestinationTower(event.currentTarget);
    }
}
const releaseToDestinationTower = function (releaseDisk) {
    let lastElement = releaseDisk.lastElementChild;
    if (!lastElement) {
        releaseDisk.appendChild(diskHolder);
        playerMode = "Pickup";
    } else {
        let currentPickedDiskWidth = lastElement.clientWidth;
        let lastPickedDiskWidth = diskHolder.clientWidth;
        if (currentPickedDiskWidth <= lastPickedDiskWidth) {
            alert("NO NO NO! Disk too big, try another tower.");
            playerMode = "Pickup";
        } else {
            releaseDisk.appendChild(diskHolder);
            playerMode = "Pickup"
        }
    }
    win();
}

const win = function () {
    if (tower_1.childElementCount === 0 && tower_3.childElementCount === 4) {
        alert('You Won!')
    }
}
const towerEventListener = function () {
    tower_1.addEventListener('click', isPickupIsputToDestinationTower)
    tower_2.addEventListener('click', isPickupIsputToDestinationTower)
    tower_3.addEventListener('click', isPickupIsputToDestinationTower)
}
towerEventListener();
